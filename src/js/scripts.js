/*
 ------------------------------- read more button ----------------------------------------
*/
var readBtn = document.getElementsByClassName("article-link");
var i;

for (i = 0; i < readBtn.length; i++) {
    readBtn[i].addEventListener("click", function () {
        let text = this.previousElementSibling;
        if (text.style.height == "100%") {
            text.style.height = "5rem";
            text.style.overflow = "hidden";
            this.textContent = "Red More";
        } else {
            text.style.height = "100%";
            text.style.overflow = "visible";
            this.textContent = "Red less";
        }
    });
}
/*
 ------------------------------- read more button ----------------------------------------
*/

/*
 ------------------------------- ORIGINAL CODE ----------------------------------------
*/
/* class Slider {
    constructor(el) {
        this.el = el;
        this.slides = document.querySelectorAll(".slide-item");
        this.prevSlide = null;
        
        this.slideIndex = Utils.getRandomNumber(0, this.slides.length - 1);
        this.showSlide(this.slideIndex);
        this.startSlideShow();
    }
    
    showSlide(index) {
        if(!this.prevSlide) {
            this.prevSlide = this.slides[index];
        }
        
        const slide = this.slides[index];
        
        this.prevSlide.classList.remove("active");
        slide.classList.add("active");
        this.prevSlide = slide;
    }
    
    startSlideShow() {
        setInterval(() => {
            this.slideIndex++;
            if(this.slideIndex >= this.slides.length) {
                this.slideIndex = 0;
            }
            
            this.showSlide(this.slideIndex);
        }, 2000);
    }
    
    static init () {
        new Slider(document.body);
    }
}

class Utils {
    static getRandomNumber(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }
}

window.onload = () => Slider.init(); */

/*
 ------------------------------- ORIGINAL CODE ----------------------------------------
*/



/*
 ------------------------------- CODE TRANSPILED USING BABEL --------------------------
*/

"use strict";

var _createClass = function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
        }
    }
    return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);
        if (staticProps) defineProperties(Constructor, staticProps);
        return Constructor;
    };
}();

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

var Slider = function () {
    function Slider(el) {
        _classCallCheck(this, Slider);

        this.el = el;
        this.slides = document.querySelectorAll(".slide-item");
        this.prevSlide = null;

        this.slideIndex = Utils.getRandomNumber(0, this.slides.length - 1);
        this.showSlide(this.slideIndex);
        this.startSlideShow();
    }

    _createClass(Slider, [{
        key: "showSlide",
        value: function showSlide(index) {
            if (!this.prevSlide) {
                this.prevSlide = this.slides[index];
            }

            var slide = this.slides[index];

            this.prevSlide.classList.remove("active");
            slide.classList.add("active");
            this.prevSlide = slide;
        }
    }, {
        key: "startSlideShow",
        value: function startSlideShow() {
            var _this = this;

            setInterval(function () {
                _this.slideIndex++;
                if (_this.slideIndex >= _this.slides.length) {
                    _this.slideIndex = 0;
                }

                _this.showSlide(_this.slideIndex);
            }, 2000);
        }
    }], [{
        key: "init",
        value: function init() {
            new Slider(document.body);
        }
    }]);

    return Slider;
}();

var Utils = function () {
    function Utils() {
        _classCallCheck(this, Utils);
    }

    _createClass(Utils, null, [{
        key: "getRandomNumber",
        value: function getRandomNumber(min, max) {
            return Math.floor(Math.random() * (max - min) + min);
        }
    }]);

    return Utils;
}();

window.onload = function () {
    return Slider.init();
};

/*
 ------------------------------- CODE TRANSPILED USING BABEL --------------------------
*/
