<?php
// This is to add style.css and scripts.js to our website
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

function add_theme_scripts() {
 
 wp_enqueue_style( 'style', get_stylesheet_uri() );
 
  wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array ( 'jquery' ), 1.1, true);
 
    }

// This is to add fontawesome library to use their icons
add_action('wp_enqueue_scripts', 'tthq_add_custom_fa_css');

function tthq_add_custom_fa_css() {
    wp_enqueue_style('custom-fa', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css');
}

// This is add the support of futured image
add_theme_support( 'post-thumbnails' );

// to get the total number of pages
function pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'mid_size' => 2,
            'current' => $current_page,
            'total' => $total_pages,
            'prev_text' => __('&laquo;', 'textdomain'),
            'next_text' => __('&raquo;', 'textdomain'),
        ));
    } 
}

// to highlight the keywords on search bar
function wps_highlight_results($text){
     if(is_search()) {
        $sr = get_query_var('s');
        $keys = explode(" ",$sr);
        $text = preg_replace('/('.implode('|', $keys) .')/iu', '<strong class="search-result">'.$sr.'</strong>', $text);
     }
     return $text;
}
add_filter('the_excerpt', 'wps_highlight_results');
add_filter('the_title', 'wps_highlight_results');
add_filter('the_content', 'wps_highlight_results');
?>
