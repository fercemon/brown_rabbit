<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name');?></title>
    <?php wp_head(); ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>

<body>
    <div class="wrapp">
        <header class="header clearfix">
            <a href="http://fernandoceres.byethost7.com/wordpress" class="logo-container">
                <img src="http://localhost/wordpress/wp-content/uploads/2019/04/logo.png" alt="logo" class="logo">
            </a>
            <nav class="nav-container">
                <div class="search-bar-container">
                        <?php get_search_form(); ?>
                </div>
                <div class="nav-links-container">
                    <a href="#" class="nav-link">ABOUT NBC</a>
                    <a href="#" class="nav-link">2011 EVENT</a>
                    <a href="#" class="nav-link">NORDIC ROASTER</a>
                    <a href="#" class="nav-link">RESULTS</a>
                    <a href="#" class="nav-link">LINKS</a>
                    <a href="#" class="nav-link">CONTACT</a>
                </div>
            </nav>
        </header>

        <section class="hero clearfix">
            <div class="slider-container">
                <img src="http://fernandoceres.byethost7.com/wordpress/wp-content/uploads/2019/04/slide5.png" class="slide-item">
                <img src="http://fernandoceres.byethost7.com/wordpress/wp-content/uploads/2019/04/slide4.png" class="slide-item">
                <img src="http://fernandoceres.byethost7.com/wordpress/wp-content/uploads/2019/04/slide3.png" class="slide-item">
                <img src="http://fernandoceres.byethost7.com/wordpress/wp-content/uploads/2019/04/slide2.png" class="slide-item">
                <img src="http://fernandoceres.byethost7.com/wordpress/wp-content/uploads/2019/04/slide1.png" class="slide-item">
            </div>
            <div class="hero-content">
                <p class="quote-text">
                    “To create an environment in which knowledge about coffee and its sphere can be obtained”
                </p>
                <div class="social-links">
                    <a class="social-media-link" href="#"><i class="fab fa-twitter fa-2x" aria-hidden="true"></i></a>
                    <a class="social-media-link" href="#"><i class="fab fa-facebook-f fa-2x" aria-hidden="true"></i></a>
                    <a class="social-media-link" href="#"><i class="fa fa-rss fa-2x" aria-hidden="true"></i></a>
                    <a class="social-media-link" href="#"><i class="fas fa-fire-alt fa-2x" aria-hidden="true"></i></a>
                    <a class="social-media-link" href="#"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></a>
                </div>
            </div>
        </section>

        <main class="main-content clearfix">
            <div class="articles-conteiner" id="articles-conteiner">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div id="posts-container">
                    <article class="article">
                       <img class="article-image" src="<?php the_post_thumbnail_url(); ?>" alt="post image">
                        <h2 class="article-headline"><?php the_title(); ?></h2>
                        <a href="#" class="article-date">Posted: <?php echo get_the_date( 'd / m-Y' ); ?> </a>
                        <div class="article-text"><?php the_content(); ?></div>
                        <button class="article-link">Read more </button>
                    </article>
                </div>
                <?php endwhile; else : ?>
                <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
                <?php
$current_page = max(1, get_query_var('paged'));
            ?>
                <div class="articles-pagination">
                    <span class="label-pages">Page <?php echo $current_page ?> of 5 </span>
                    <?php pagination_bar(); ?>
                </div>
            </div>
            <aside class="extra-information">
                <div class="shop-wrapper">
                    <h2 class="extra-information-headline shop-headline">NBC Shop</h2>
                    <div class="extra-information-body">
                        <div>Your shopping cart is empty</div>
                        <a class="extra-information-links" href="#">
                            Visit the shop
                        </a>
                    </div>
                </div>
                <div class="next-event-wrapper">
                    <h2 class="extra-information-headline next-event">Next Event</h2>
                    <div class="extra-information-body">
                        <div>NORDIC BARISTA CUP 2011</div>
                        <div>Copenhagen, Denmark</div>
                        <div>Dates : 25th - 27th August 2011</div>
                        <div>Theme : SENSORY</div>
                        <a class="extra-information-links sign-up-link" href="#">
                            Sign up now.
                        </a>
                    </div>
                </div>
                <div class="scoreboard-wrapper">
                    <h2 class="extra-information-headline">Scoreboard</h2>
                    <div class="extra-information-body">
                        <div class="list-headline">List of winners from past years</div>
                        <ul class="extra-information-unordered-list">
                            <li>2011 - ?</li>
                            <li>2010 - Sweden</li>
                            <li>2009 - Denmark</li>
                            <li>2007 - Sweden</li>
                            <li>2006 - Norway</li>
                            <li>2005 - Norway</li>
                            <li>2004 - Denmark</li>
                        </ul>
                    </div>
                </div>
            </aside>
        </main>

        <section class="sponsors">
            <h2 class="sponsors-headline">Nordic Barista Cup Sponsors</h2>
            <div class="sponsors-container">
                <img class="frame-img" src="http://localhost/wordpress/wp-content/uploads/2019/04/Frame.png" alt="logo frame">
            </div>
        </section>

        <footer class="clearfix">
            <div class="about-footer">
                <h2 class="footer-headline">About Nordic Barista Cup</h2>
                <p>The vision within the Nordic Barista Cup is:</p>
                <p class="footer-highlight-quote">
                    “To create an environment in which knowledge about coffee and its sphere can be obtained”
                </p>
                <p class="footer-quote">
                    ...create an environment…’ <br>
                    Combined with personally absorption having the opportunity to see and experience countries, people, traditions etc. will always serve as a source of inspiration in our daily work. <br>
                    The organization behind the Nordic Barista Cup see it as its main purpose to be a part of creating this forum in which people can meet, bond and achieve further knowledge.
                </p>
            </div>
            <div class="flickr-stream">
                <h2 class="footer-headline">NBC Flickr Stream</h2>
                <img class="flickr-img" src="http://fernandoceres.byethost7.com/wordpress/wp-content/uploads/2019/04/flickr-stream.png" alt="flickr stream">
            </div>
            <div class="contact-footer">
                <h2 class="footer-headline">Contact</h2>
                <p class="footer-contact-event-name">Nordic Barista Cup</p>
                <div class="footer-contact-details-wrapper">
                    <p class="footer-contact-details">Amagertorv 13</p>
                    <p class="footer-contact-details">1160 Copenhagen K</p>
                    <p class="footer-contact-details">+45 33 12 04 28</p>
                    <p class="footer-contact-details">CVR: 11427693</p>
                    <p class="footer-contact-details">Email: bbrend@nordicbaristacup.com</p>
                </div>
                <div class="footer-social-links">
                    <a class="social-media-link" href="#"><i class="fab fa-twitter fa-2x" aria-hidden="true"></i></a>
                    <a class="social-media-link" href="#"><i class="fab fa-facebook-f fa-2x" aria-hidden="true"></i></a>
                    <a class="social-media-link" href="#"><i class="fa fa-rss fa-2x" aria-hidden="true"></i></a>
                    <a class="social-media-link" href="#"><i class="fas fa-fire-alt fa-2x" aria-hidden="true"></i></a>
                    <a class="social-media-link" href="#"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></a>
                </div>
            </div>
        </footer>
    </div>
    <?php wp_footer(); ?>
    
</body>

</html>
