
<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <div>
        <label class="screen-reader-text" for="s"><i class="fas fa-search search-icon"></i>
                        <i class="fas fa-angle-down search-icon"></i></label>
        <input value="" name="s" id="s" type="text" class="search-bar-input">
    </div>
</form>